/**
 *
 *
 * @author Tatevosyan Artem (@teqst)
 */

(() => {
    const allTabs = document.querySelectorAll('.tabs__control-item');
    const allTabsContent = document.querySelectorAll('.tabs__content-item');

    if(!allTabs || !allTabsContent) return;

    allTabs.forEach(tab => {
        tab.addEventListener('click', showTab);
    })

    function showTab(e) {
        allTabs.forEach(i => i.classList.remove('tabs__control-item--active'));
        e.target.classList.toggle('tabs__control-item--active');
        hideTab(e.target.dataset.tabname);
    }

    function hideTab(tabName) {
        allTabsContent.forEach(i => {
            if (tabName === i.dataset.tabname) {
                i.classList.add('tabs__content-item--active');
            } else {
                i.classList.remove('tabs__content-item--active');
            }
        });
    }

})();