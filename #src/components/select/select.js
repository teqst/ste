/**
 *
 *
 * @author Tatevosyan Artem (@teqst)
 */

class CustomSelect {
    constructor(settings) {
        this.customSelectClass = settings.customSelectClass;
        this.multiple = settings.multiple;
        this.changeState = settings.changeState;
        this.init();
    }

    customSelectEvent = new Event('customSelectChange');
    selectSelected = null;
    selectList = null;

    createSelectedElement() {
        this.selectSelected = document.createElement("div");
        this.selectSelected.setAttribute("class", "select-selected");
        this.selectSelected.innerHTML = this.nativeSelect.options[this.nativeSelect.selectedIndex].innerHTML;
        return this.selectSelected;
    }

    createOptionsList() {
        this.selectList = document.createElement("div");
        this.selectList.setAttribute("class", "select-items select-hide");
        return this.selectList;
    }

    toggleSelectList() {
        this.selectSelected.addEventListener('click', (event) => {
            this.closeAllSelects(this.selectSelected.parentNode);
            this.selectSelected.classList.toggle('select-arrow-active');
            this.selectList.classList.toggle('select-hide');
        })
    }

    closeAllSelects(el) {
        const allSelects = document.querySelectorAll('.custom-select');

        for(let select of allSelects) {
            if (el !== select) {
                select.querySelector('.select-selected').classList.remove('select-arrow-active');
                select.querySelector('.select-items').classList.add('select-hide');
            }
        }
    }

    init() {
        this.customSelect = document.querySelector(this.customSelectClass);
        this.nativeSelect = this.customSelect.querySelector('select');
        if (this.multiple) this.nativeSelect.setAttribute('multiple', true);

        this.customSelect.append(this.createSelectedElement());

        this.createOptionsList();

        for(let i = 1; i < this.nativeSelect.length; i++) {
            let option = document.createElement("div");

            if(this.customSelectClass.match(/#color\d+/g) || this.customSelectClass === '.custom-select--color') {
                option.value = this.nativeSelect.options[i].value;
                option.setAttribute('data-value', option.value);
                option.style.backgroundColor = option.value;
            } else {
                option.innerHTML = this.nativeSelect.options[i].innerHTML;
                option.setAttribute('data-value', option.innerText);
            }

            option.addEventListener('click', () => {
                if(this.multiple === true) {
                    if (option.classList.contains('custom-selected')) {
                        option.removeAttribute('class');
                        this.nativeSelect.options[i].removeAttribute('selected');
                    } else {
                        option.setAttribute('class', 'custom-selected');
                        this.nativeSelect.options[i].setAttribute('selected', true);
                    }
                } else {
                    let checkedOptions = option.parentNode.querySelectorAll(".custom-selected");
                    checkedOptions.forEach(opt => opt.removeAttribute('class'));

                    for(let opt of this.nativeSelect.options) {
                        opt.removeAttribute('selected');
                    }

                    this.nativeSelect.options[i].setAttribute('selected', true);
                    option.setAttribute('class', 'custom-selected');

                    if (this.changeState && (this.customSelectClass.match(/#color\d+/g) || this.customSelectClass === '.custom-select--color')) {
                        const currentColor = option.getAttribute('data-value');
                        this.selectSelected.style.backgroundColor = currentColor;
                    }
                    else if (this.changeState) {
                        this.selectSelected.textContent = this.nativeSelect.options[i].textContent;
                    }
                }

                    this.selectSelected.classList.toggle('select-arrow-active');
                    this.selectList.classList.toggle('select-hide');
                    this.customSelect.dispatchEvent(this.customSelectEvent);
            })

            this.selectList.append(option);
        }

        this.customSelect.append(this.selectList);

        this.toggleSelectList();

        document.addEventListener('keydown', (e) => {
            if (e.key === 'Escape') {
                this.closeAllSelects(false);
            }
        })
    }
}


(() => {

    const filters = document.querySelectorAll('.custom-select--color, .custom-select--size, .custom-select--filter, .custom-select--sort');

    if(filters.length != 4) return;

    new CustomSelect({
        customSelectClass: '.custom-select--filter',
        multiple: true,
    });

    new CustomSelect({
        customSelectClass: '.custom-select--sort',
        multiple: false,
    });

    new CustomSelect({
        customSelectClass: '.custom-select--size',
        multiple: true,
    });

    new CustomSelect({
        customSelectClass: '.custom-select--color',
        multiple: true,
    });


    // [...filters].forEach(filter => {
    //     filter.addEventListener('customSelectChange', (e) => {
    //         const nSelect = e.target.childNodes[1];
    //         for(let i = 0; i < nSelect.length; i++) {
    //             console.log(nSelect.options[i].attributes)
    //         }
    //     })
    // })
})();

(() => {

    const datebirth = document.querySelectorAll('.custom-select--day, .custom-select--month, .custom-select--year');

    if(!datebirth.length) return;

    new CustomSelect({
        customSelectClass: '.custom-select--day',
        multiple: false,
        changeState: true,
    });

    new CustomSelect({
        customSelectClass: '.custom-select--month',
        multiple: false,
        changeState: true,
    });

    new CustomSelect({
        customSelectClass: '.custom-select--year',
        multiple: false,
        changeState: true,
    });

})();

(() => {
    const cartSelectsColor = document.querySelectorAll("[id^=color]");
    const cartSelectsSize = document.querySelectorAll("[id^=size]");

    if (!cartSelectsColor.length && !cartSelectsSize) return;

    cartSelectsColor.forEach((s) => {
        new CustomSelect({
            customSelectClass: '#' + s.getAttribute('id'),
            multiple: false,
            changeState: true,
        });
    })

    cartSelectsSize.forEach((s) => {
        new CustomSelect({
            customSelectClass: '#' + s.getAttribute('id'),
            multiple: false,
            changeState: true,
        });
    })

})();