/**
 *
 *
 * @author Tatevosyan Artem (@teqst)
 */

(() => {
    const slider = document.querySelector('.js-slider-four');
    const sliderNextEl = '.button--slider-next',
        sliderPrevEl = '.button--slider-prev';

    const sliderThree = document.querySelector('.js-slider-three');
    const sliderNextElThree = '.button--slider-next-three',
        sliderPrevElThree = '.button--slider-prev-three';

    let swiperSlider, swiperSliderThree = null;

    if (!slider) return;

    const initSlider = () => {
        swiperSlider = new Swiper(slider, {
            slidesPerView: 4,
            spaceBetween: 18,
            navigation: {
                nextEl: sliderNextEl,
                prevEl: sliderPrevEl,
            }
        });

        swiperSliderThree = new Swiper(sliderThree, {
            slidesPerView: 3,
            spaceBetween: 18,
            navigation: {
                nextEl: sliderNextElThree,
                prevEl: sliderPrevElThree,
            }
        });
    };

    document.addEventListener('DOMContentLoaded', initSlider);

})();
