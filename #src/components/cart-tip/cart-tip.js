/**
 *
 *
 * @author Tatevosyan Artem (@teqst)
 */

(() => {
    const cartTip = document.querySelector('.cart-tip');
    const menuCart = document.querySelector('.menu__action-item--cart');

    if (!cartTip || !menuCart) return;

    const menuCartPos = menuCart.getBoundingClientRect();

    function mouseEventHandler(opt) {
        if (opt) {
            cartTip.style.left = `${menuCartPos.left - 310}px`;
            cartTip.style.top = '18px';
        } else {
            cartTip.style.top = '-99999px';
        }
    }

    menuCart.addEventListener('mouseover', () => {
        mouseEventHandler(true);
    });

    window.addEventListener('mouseover', (e) => {
        if (!e.target.closest('.cart-tip') && !e.target.closest('.menu__action-list')) {
            mouseEventHandler(false);
        }
    })
})();