/**
 *
 *
 * @author Tatevosyan Artem (@teqst)
 */

(() => {
    const menuSearch = document.querySelector('.menu__action-item--search');
    const menuContacts = document.querySelector('.menu__contacts');
    const menuSearchInput = document.querySelector('.menu__action-input');

    if (!menuSearch) return;

    function closeSearch() {
        menuSearch.classList.remove('menu__action-item--search-active');
        menuContacts.style.opacity = '1';
        menuContacts.style.appearance = 'visible';

    }

    function openSearch() {
        menuSearch.classList.add('menu__action-item--search-active');
        menuContacts.style.opacity = '0';
        menuContacts.style.appearance = 'none';
        // menuSearchInput.focus();
    }

    menuSearch.addEventListener('click', openSearch);

    window.addEventListener('click', (e) => {
        if (!e.target.closest('.menu__action-item--search') ) {
            closeSearch();
        }
    })

})();