/**
 *
 *
 * @author Tatevosyan Artem (@teqst)
 */

(() => {
    const checkoutButton = document.querySelector('.cart__actions-btn--checkout');
    const checkoutBlock = document.querySelector('.checkout');

    if (!checkoutButton) return;

    checkoutButton.addEventListener('click', () => {
        checkoutBlock.scrollIntoView()
    });

})();