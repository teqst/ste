(() => {
    const unselectButtons = document.querySelectorAll('.js-button-unselect');

    if (!unselectButtons.length) return;

    unselectButtons.forEach(btn => {
        btn.addEventListener('click', () => {
            btn.parentNode.remove();
        })
    })
})();