# "STE" Website ***(markup)***

### Деплой
* `npm install` > (enter command at terminal) `gulp` > All set up!

### Команды
* `gulp`

### Стек
* Babel в качестве транслятора ECMAScript6;
* Imagemin - сжатие png, jpeg, gif, svg;
* Autoprefixer - префиксер для кроссбраузерности;
* CleanCSS - CSS minify;
* FileInclude - инклюдим группы в один файл;
* BrowserSync - для лайв-релоуд;
* Сборка на [Gulp](http://gulpjs.com) (v4.0.2);
* Шаблонизатор [Pug](https://pugjs.org/api/getting-started.html);
* SCSS для написания CSS [SCSS](https://sass-scss.ru/)


### Файловая структура проекта
* assets
    * fonts - *используемые шрифты*
    * libs - *Javascript библиотеки*
    * scripts - *общие скрипты*
    * styles
        * common
            * **base.scss** - *общие настроечные стили*
            * **destyle.scss** - *сброс стандартной таблицы стилей*
            * **fonts.scss** - *подключение шрифтов*
            * **variables.scss** - *переменные*
        * utils
            * **helpers.scss** - *классы-хелперы*
        * vendor - *файлы стилей вендоров*
        * **main.scss** - *файл, для подключения всех стилей в единый*
    * components - *компоненты (миксины), из которых собираем страницы*
        * **[component_folder_name]**
            * **[component_name].js** - *скрипт компонента*
            * **[component_name].scss** - *стили компонента*
            * **[component_name].pug** - *разметка *
    * templates
        * layouts - *шаблоны layoutов (мастер-страницы)*
            * ***.pug** - *layout файлы *
        * pages - *папка со страницами*
            * ***.pug** - *страницы, собираемые из компонентов*
    * dist - *собранный проект*
    * **gulpfile.js** - *конфиг. файл сборки*